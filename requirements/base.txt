-r defaults.txt

# Django
# ------------------------------------------------------------------------------
Django==3.0
django-crispy-forms==1.8.1  # https://github.com/django-crispy-forms/django-crispy-forms
djangorestframework==3.11.0  # https://www.django-rest-framework.org/
django-material-admin==1.6.44  # https://github.com/MaistrenkoAnton/django-material-admin