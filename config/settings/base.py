"""
Generated by 'django-admin startproject' using Django 1.11.13.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

from .defaults import *

INSTALLED_APPS += [
    "core",
    "users",
    "matches",
    "goals",
    "players",
]

TEMPLATES[0]['OPTIONS']['context_processors'] += [
    'core.context_processors.project',
]

################################################################################
#                            THIRD PARTY MODULES                               #
################################################################################

# DJANGO EXTENSIONS
# ------------------------------------------------------------------------------
# https://django-extensions.readthedocs.io/en/latest/command_extensions.html

INSTALLED_APPS += ['django_extensions']

# DJANGO REST FRAMEWORK
# ------------------------------------------------------------------------------
# https://www.django-rest-framework.org

INSTALLED_APPS += ['rest_framework']
REST_FRAMEWORK = {
    'DEFAULT_PERMISSION_CLASSES': (
        'rest_framework.permissions.IsAdminUser',
    ),
}

