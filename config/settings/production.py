"""
Settings for production. Inherit from config.settings.base.
"""
from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY')
DEBUG = False

ALLOWED_HOSTS = [
    f".{env('DOMAIN')}",
]

CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
