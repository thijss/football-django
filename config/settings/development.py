"""
Settings for development. Inherit from config.settings.base.
"""

from .base import *

SECRET_KEY = 'development'

ALLOWED_HOSTS = ['*']

DEBUG = True
