"""serializers"""
from rest_framework import serializers

from matches.models import Match


class MatchSerializer(serializers.ModelSerializer):
    """Serializer for matches."""

    class Meta:
        model = Match
        fields = [
            'uuid',
            'date',
            'home',
            'score',
            'result']


class PlayerMatchListSerializer(serializers.ModelSerializer):
    """Serializer for matches of a player."""
    player_goals = serializers.SerializerMethodField()

    class Meta:
        model = Match
        fields = [
            'uuid',
            'date',
            'home',
            'opponent',
            'player_goals'
        ]

    def get_player_goals(self, obj):
        """Returns all goals scored by player during the match."""
        return obj.goals.filter(player=self.root.instance).count()
