"""serializers"""
from rest_framework import serializers
from players.models import Player

from .constants import PLAYER_BASE_FIELDS
from ..matches.serializers import PlayerMatchListSerializer


class PlayerListSerializer(serializers.ModelSerializer):
    """Serializer for player list."""

    class Meta:
        model = Player
        fields = PLAYER_BASE_FIELDS


class PlayerDetailSerializer(serializers.ModelSerializer):
    """Serializer for player detail."""

    matches = PlayerMatchListSerializer(many=True, read_only=True)

    class Meta:
        model = Player
        fields = PLAYER_BASE_FIELDS + [
            'matches',
        ]
