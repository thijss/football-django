"""Constants for player api."""

PLAYER_BASE_FIELDS = [
    'uuid',
    'name',
    'total_goals',
]
